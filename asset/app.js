
angular
        .module('Decision', [])
        .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEnter);
                        });
                        event.preventDefault();
                    }
                });
            };
        })
        .directive('ngEsc', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 27) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEsc);
                        });
                        event.preventDefault();
                    }
                });
            };
        })
        .directive('ngLeft', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 37 && scope.current.submitted) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngLeft);
                        });
                        event.preventDefault();
                    }
                });
            };
        })
        .directive('ngRight', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 39 && scope.current.submitted) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngRight);
                        });
                        event.preventDefault();
                    }
                });
            };
        })
        .directive('ngUp', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 38) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngUp);
                        });
                        event.preventDefault();
                    }
                });
            };
        })
        .directive('ngDown', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 40) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngDown);
                        });
                        event.preventDefault();
                    }
                });
            };
        })
        .controller('MainController', function ($scope) {

            $scope.title = '';
            $scope.current = {
                title: '',
                significance: 0,
                submitted: false
            };
            $scope.args = {plus: [], minus: []};

            $scope.clone = function (obj) {
                if (null === obj || "object" !== typeof obj)
                    return obj;
                var copy = obj.constructor();
                for (var attr in obj) {
                    if (obj.hasOwnProperty(attr))
                        copy[attr] = obj[attr];
                }
                return copy;
            };

            $scope.clr = function () {
                $scope.current.title = '';
                $scope.current.significance = 0;
                $scope.current.submitted = false;
            };

            $scope.plus = function () {
                if ($scope.current.submitted) {
                    $scope.args.plus.push($scope.clone($scope.current));
                    $scope.clr();
                }
            };

            $scope.minus = function () {
                if ($scope.current.submitted) {
                    $scope.args.minus.push($scope.clone($scope.current));
                    $scope.clr();
                }
            };

            $scope.up = function () {
                if ($scope.current.significance < 10) {
                    $scope.current.significance += 1;
                }
            };
            $scope.down = function () {
                if ($scope.current.significance > -10) {
                    $scope.current.significance -= 1;
                }
            };

            $scope.submit = function (el) {
                if ($scope.current.title === '') {
                    return;
                }
                $scope.current.submitted = true;
            };

            $scope.calc = function () {
		var plus = $scope.args.plus.length, minus = $scope.args.minus.length;
		angular.forEach($scope.args.plus, function(a, i){
		    plus += a.significance;
		});
		angular.forEach($scope.args.minus, function(a, i){
		    minus += a.significance;
		});
		if (plus === minus) {
		    alert('Вы пока не готовы принять решение');
		} else {
		    var 
		    d = minus === 0 ? 1 : Math.abs(plus)/Math.abs(minus),
		    nd = d === 0 ? 1 : 1/d; 
		    if (plus > minus) {
			if (d >= 1.5 ) {
			    alert('Определенно, "Да"');
			} else {
			    alert('Скорее "Да", чем "Нет"');
			}
		    } else {
			if (nd >= 1.5 ) {
			    alert('Определенно, "Нет"');
			} else {
			    alert('Скорее "Нет", чем "Да"');
			}
		    }
		} 
            };





        });