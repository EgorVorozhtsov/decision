<!DOCTYPE html>
<html ng-app="Decision">
    <head>
        <title>Примите взвешенное решение</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="//cdn.vorozhtsov.com.ru/static/sandbox/decision/styles.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    </head>
    <body ng-controller="MainController as mainCtrl" onload="document.getElementById('problem-name').focus()">
        <div id="wrapper">
            <div id="head">
                <input id="problem-name" placeholder="Введите Ваш вопрос" type="text" ng-model="title" ng-esc="title=''">
                <hr>
                <div id="problem-arg">
					<input
						type="text"
						placeholder="приводите доводы"
						ng-model="current.title"
						ng-enter="submit(this);"
						ng-esc="clr()"
						ng-left="plus()"
						ng-right="minus()"
						ng-up="up()"
						ng-down="down()"
						> <span class="badge">{{current.significance}}</span>
                </div>
            </div>
            <div id="plus">
                <ul class="list-unstyled" ng-repeat="arg in args.plus">
                    <li class="alert alert-success">
                        <span class="pull-left">{{arg.title}}</span>&nbsp;
                        <span class="pull-right">{{arg.significance}}</span>
                    </li>
                </ul>
            </div>
            <div id="minus">
                <ul class="list-unstyled" ng-repeat="arg in args.minus">
                    <li class="alert alert-danger">
                        <span class="pull-right">{{arg.title}}</span>&nbsp;
                        <span class="pull-left">{{arg.significance}}</span>
                    </li>
                </ul>
            </div>
            <div id="footer">
                <div class="col-md-10">
                    Введите аргумент, 
					нажмите <span class="btn btn-sm btn-default">Enter</span>, 
					клавишами <span class="btn btn-sm btn-default"><i class="fa fa-long-arrow-up"></i></span> и 
					<span class="btn btn-sm btn-default"><i class="fa fa-long-arrow-down"></i></span> присвойте вес, 
					<span class="btn btn-sm btn-default"><i class="fa fa-long-arrow-left"></i></span> и 
					<span class="btn btn-sm btn-default"><i class="fa fa-long-arrow-right"></i></span> - позицию. 
					<span class="btn btn-sm btn-default">Esc</span> - очистка.
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary pull-right" ng-click="calc()">Решить!</button>
                </div>
            </div>
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0-beta.5/angular.min.js"></script>
        <script src="//cdn.vorozhtsov.com.ru/static/sandbox/decision/app.js"></script>
    </body>
</html>
